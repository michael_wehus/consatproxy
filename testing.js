var http = require('http'),
    httpProxy = require('http-proxy'),
    proxy = httpProxy.createProxyServer({}),
    url = require('url');
proxy.on('proxyReq', function(proxyReq, req, res, options) {
  var pathname = url.parse(req.url).pathname;
  if (pathname.startsWith("/akt")) proxyReq.setHeader('host', 'its4mobility.pw');
});
proxy.on('error', function (err, req, res) {
  res.writeHead(500, {
    'Content-Type': 'text/plain'
  });

  res.end('Something went wrong. The server is most proably offline');
});
http.createServer(function(req, res) {
    //var hostname = req.headers.host.split(":")[0];
    var pathname = url.parse(req.url).pathname;

    //console.log(hostname);
    console.log(pathname);
    if (pathname.startsWith("/akt")) {
      console.log("Hi AKT");
      proxy.web(req, res, { target: 'http://its4mobility.pw:80' });
    } else if (pathname.startsWith("/micsen")) {
		console.log("Micsen i see you");
		proxy.web(req, res, { target: 'http://localhost:8080' });
	} else {
      proxy.web(req, res, { target: 'http://83.145.60.18:80' });
    }
}).listen(80, function() {
    console.log('proxy listening on port 80');
});
